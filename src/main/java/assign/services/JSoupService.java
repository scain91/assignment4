package assign.services;

import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Sara on 3/13/2015.
 */
public interface JSoupService {
    public Elements getElements(String source);

    public ArrayList<String> getLinkExtensions(Elements links);

    public boolean projectExists(String origsource, String projectName);


}
