package assign.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

public class JSoupHandlerServiceImpl implements JSoupService {

    public JSoupHandlerServiceImpl() {}

    @Override
	public Elements getElements(String source) {
	    Document doc = null;
		try {
			doc = Jsoup.connect(source).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    Elements links = doc.select("body a");
        //Elements correctLinks = links.contains()
	    return links;
	}

    @Override
    public ArrayList<String> getLinkExtensions(Elements links) {
        ListIterator<Element> iter = links.listIterator();
        ArrayList<String> projects = new ArrayList<String>();
        // Code path:
        boolean seenParentDir = false;
        while(iter.hasNext()) {
            Element e = (Element) iter.next();
            String s = e.html();
            if (s != null) {
                s = s.replace("#", "%23");
                if (seenParentDir) {
                    System.out.println("s: " + s);
                    projects.add(s);
                } else if (!seenParentDir) {
                    if (s.contains("Parent Directory")) {
                        System.out.println("s: " + s);
                        seenParentDir = true;
                    }
                }
            }
        }
        return projects;
    }

    @Override
    public boolean projectExists(String origsource, String projectName) {
        Elements meetingslinks = getElements(origsource);
        ArrayList<String> possiblelinks = getLinkExtensions(meetingslinks);
        for(String l : possiblelinks) {
            if(l.equals(projectName)) {
                return true;
            }
        }
        return false;
    }


}