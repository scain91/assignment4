package assign.services;

import org.jsoup.select.Elements;

import java.util.ArrayList;

public class EavesdropService {

    JSoupHandlerServiceImpl jSoupHandlerService = new JSoupHandlerServiceImpl();

    public String getData() {
        return "Hello from Eavesdrop service.";
    }

    public ArrayList<String> getProjects(String url) {
        ArrayList<String> projectList = new ArrayList<String>();
        //make call to link and parse html to get project names

        Elements elements = jSoupHandlerService.getElements(url);
        projectList = jSoupHandlerService.getLinkExtensions(elements);
        return projectList;
    }


    public ArrayList<String> getProjectLinks(String url) {
        System.out.println("Printing for project url: " + url);
        //url = http://eavesdrop.openstack.org/irclogs/%23heat
        //links should be: <link>http://eavesdrop.openstack.org/irclogs/%23heat/#heat.2013-12-10.log</link>
        //                             http://eavesdrop.openstack.org/irclogs/%23heat/#heat.2013-12-10.log
        //links are originally: <link>%23heat.2013-12-10.log</link>
        ArrayList<String> projectLinks = new ArrayList<String>();
        Elements elements = jSoupHandlerService.getElements(url);
        projectLinks = jSoupHandlerService.getLinkExtensions(elements);
        ArrayList<String> outputLinks = new ArrayList<String>();
        for(String link : projectLinks) {
            link = link.substring(3);
            link = url + "/#" + link;
            //System.out.println("New link: " + link);
            outputLinks.add(link);
        }
        return outputLinks;
    }
}