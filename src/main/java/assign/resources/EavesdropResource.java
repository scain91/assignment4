package assign.resources;

import assign.domain.Project;
import assign.domain.Projects;
import assign.services.EavesdropService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Sara on 4/4/2015.
 */

@Path("/myeavesdrop/projects")
public class EavesdropResource {

    EavesdropService eavesdropService;

    public EavesdropResource() {
        this.eavesdropService = new EavesdropService();
    }

    @GET
    @Path("/helloworld")
    @Produces("text/html")
    public String helloWorld() {
        return "Hello world Sara's eavesdrop app git";
    }

    //GET http://localhost:8080/assignment4/myeavesdrop/projects/{project}/irclogs
    //return http://eavesdrop.openstack.org/irclogs/project/
    //assignment4/myeavesdrop/projects/#heat/irclogs
    @GET
    @Path("/{projectName}/irclogs")
    @Produces("application/xml")
    public StreamingOutput getProject(@PathParam("projectName") String projectName) throws Exception {
        System.out.println("INSIDE " + projectName + " CALL");
        //replace %23 with #
        projectName = projectName.substring(1);
        projectName = "%23" + projectName;
        System.out.println("INSIDE " + projectName + " CALL");


        //get list of irclogs projects, check if project exists
        String irclogsURL = "http://eavesdrop.openstack.org/irclogs/";
        ArrayList<String> irclogsProjects = eavesdropService.getProjects(irclogsURL);
        final Project project = new Project();

        if(irclogsProjects.contains(projectName + "/")) {
            //make get call to http://eavesdrop.openstack.org/irclogs/project/
            //create Project object and add links to links it
            //links should be: <link>http://eavesdrop.openstack.org/irclogs/%23heat/#heat.2013-12-10.log</link>
            //links are originally: <link>%23heat.2013-12-10.log</link>
            System.out.println("Project in irclogs");
            ArrayList<String> projectLinks = eavesdropService.getProjectLinks(irclogsURL+projectName);
            project.setName(projectName);
            project.setLink(new ArrayList<String>());
            for(String link : projectLinks) {
                project.getLink().add(link);
            }
            for(String plink : project.getLink()) {
                System.out.println("Project link: " + plink);
            }

            //output xml of Project object to user
            return new StreamingOutput() {
                public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                    outputCourses(outputStream, project);
                }
            };
        }
        else {
            //if project does not exist return HTTP 404 Not Found response
            System.out.println("Project not in irclogs");
            /*return new StreamingOutput() {
                public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                    outputError(outputStream, "404 Not Found");
                }
            };*/
            throw new WebApplicationException(Response.Status.NOT_FOUND);

            //return Response.Status.NOT_FOUND; //entity("404 project " + project + " does not exist").build();
        }
    }

    @GET
    @Path("/")
    @Produces("application/xml")
    public StreamingOutput getUnionProjects() throws Exception {
        System.out.println("INSIDE PROJECTS CALL");

        //parse meetings for projects - http://eavesdrop.openstack.org/meetings/
        //parse irclogs for projects - http://eavesdrop.openstack.org/irclogs/
        //get union of meetings and irclogs projects
        //output projects in xml
        String meetingsURL = "http://eavesdrop.openstack.org/meetings/";
        String irclogsURL = "http://eavesdrop.openstack.org/irclogs/";
        ArrayList<String> meetingsProjects = eavesdropService.getProjects(meetingsURL);
        ArrayList<String> irclogsProjects = eavesdropService.getProjects(irclogsURL);

        ArrayList<String> unionProjects = unionProjects(meetingsProjects, irclogsProjects);
        final Projects projects = new Projects();
        projects.setProjects(new ArrayList<String>());
        for(String u : unionProjects) {
            Project temp = new Project();
            temp.setName(u);
            projects.getProjects().add(u);
        }

        /*Project heat = new Project();
        heat.setName("%23heat");

        final Projects projects = new Projects();
        projects.setProjects(new ArrayList<String>());
        projects.getProjects().add("%23heat");
        projects.getProjects().add("%23dox");*/

        return new StreamingOutput() {
            public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                outputCourses(outputStream, projects);
            }
        };
    }

    private ArrayList<String> unionProjects(ArrayList<String> meetingsProjects, ArrayList<String> irclogsProjects) {
        Set<String> projectsSet = new TreeSet<String>(Comparator.<String>naturalOrder());
        for(String strMeeting : meetingsProjects) {
            projectsSet.add(strMeeting);
        }
        for(String strIrc : irclogsProjects) {
            projectsSet.add(strIrc);
        }
        ArrayList<String> union = new ArrayList<String>();
        union.addAll(projectsSet);
        return union;
    }




    protected void outputError(OutputStream os, String error) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(String.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(error, os);
        } catch (JAXBException jaxb) {
            throw new WebApplicationException();
        }
    }

    protected void outputCourses(OutputStream os, Projects projects) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(projects, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }

    protected void outputCourses(OutputStream os, Project project) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(project, os);
        } catch (JAXBException jaxb) {
            jaxb.printStackTrace();
            throw new WebApplicationException();
        }
    }

}
