import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Sara on 4/5/2015.
 */
public class EavesdropResourceTest {

    @Test
    public void testEavesdropResourceGetUnionProjects() {
        Client client = ClientBuilder.newClient();
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<projects>\n" +
                "    <project>%23dox/</project>\n" +
                "    <project>%23heat/</project>\n" +
                "    <project>%23magnetodb/</project>\n" +
                "    <project>%23murano/</project>\n" +
                "    <project>%23openstack-ansible/</project>\n" +
                "    <project>%23openstack-api/</project>\n" +
                "    <project>%23openstack-barbican/</project>\n" +
                "    <project>%23openstack-blazar/</project>\n" +
                "    <project>%23openstack-ceilometer/</project>\n" +
                "    <project>%23openstack-chef/</project>\n" +
                "    <project>%23openstack-cinder/</project>\n" +
                "    <project>%23openstack-climate/</project>\n" +
                "    <project>%23openstack-community/</project>\n" +
                "    <project>%23openstack-containers/</project>\n" +
                "    <project>%23openstack-cue/</project>\n" +
                "    <project>%23openstack-dev/</project>\n" +
                "    <project>%23openstack-dns/</project>\n" +
                "    <project>%23openstack-doc/</project>\n" +
                "    <project>%23openstack-fr/</project>\n" +
                "    <project>%23openstack-gate/</project>\n" +
                "    <project>%23openstack-glance/</project>\n" +
                "    <project>%23openstack-horizon/</project>\n" +
                "    <project>%23openstack-infra/</project>\n" +
                "    <project>%23openstack-ironic/</project>\n" +
                "    <project>%23openstack-keystone/</project>\n" +
                "    <project>%23openstack-ko/</project>\n" +
                "    <project>%23openstack-lbaas/</project>\n" +
                "    <project>%23openstack-manila/</project>\n" +
                "    <project>%23openstack-marconi/</project>\n" +
                "    <project>%23openstack-meeting-3/</project>\n" +
                "    <project>%23openstack-meeting-4/</project>\n" +
                "    <project>%23openstack-meeting-alt/</project>\n" +
                "    <project>%23openstack-meeting/</project>\n" +
                "    <project>%23openstack-metering/</project>\n" +
                "    <project>%23openstack-monasca/</project>\n" +
                "    <project>%23openstack-neutron/</project>\n" +
                "    <project>%23openstack-nova/</project>\n" +
                "    <project>%23openstack-operators/</project>\n" +
                "    <project>%23openstack-oslo/</project>\n" +
                "    <project>%23openstack-qa/</project>\n" +
                "    <project>%23openstack-rating/</project>\n" +
                "    <project>%23openstack-relmgr-office/</project>\n" +
                "    <project>%23openstack-sahara/</project>\n" +
                "    <project>%23openstack-sdks/</project>\n" +
                "    <project>%23openstack-security/</project>\n" +
                "    <project>%23openstack-sprint/</project>\n" +
                "    <project>%23openstack-stable/</project>\n" +
                "    <project>%23openstack-summit/</project>\n" +
                "    <project>%23openstack-swift/</project>\n" +
                "    <project>%23openstack-trove/</project>\n" +
                "    <project>%23openstack-zaqar/</project>\n" +
                "    <project>%23openstack/</project>\n" +
                "    <project>%23savanna/</project>\n" +
                "    <project>%23storyboard/</project>\n" +
                "    <project>%23tripleo/</project>\n" +
                "    <project>3rd_party_ci/</project>\n" +
                "    <project>___endmeeting/</project>\n" +
                "    <project>__poppy/</project>\n" +
                "    <project>__xenapi/</project>\n" +
                "    <project>_docteam/</project>\n" +
                "    <project>_fuel/</project>\n" +
                "    <project>_hyper_v/</project>\n" +
                "    <project>_neutron_ipv6/</project>\n" +
                "    <project>_nova_api/</project>\n" +
                "    <project>_octavia_/</project>\n" +
                "    <project>_trove/</project>\n" +
                "    <project>_trove_bp_review/</project>\n" +
                "    <project>_trove_meeting_/</project>\n" +
                "    <project>ambassadors/</project>\n" +
                "    <project>api_wg/</project>\n" +
                "    <project>barbican/</project>\n" +
                "    <project>barbican_status_meeting/</project>\n" +
                "    <project>barbican_weekly_meeting/</project>\n" +
                "    <project>blazar/</project>\n" +
                "    <project>blueprint_ovs_firewall_driver/</project>\n" +
                "    <project>bp_review/</project>\n" +
                "    <project>ceilometer/</project>\n" +
                "    <project>ci/</project>\n" +
                "    <project>cinder/</project>\n" +
                "    <project>cinder__unofficial_meeting/</project>\n" +
                "    <project>climate/</project>\n" +
                "    <project>community/</project>\n" +
                "    <project>congressteammeeting/</project>\n" +
                "    <project>containers/</project>\n" +
                "    <project>crossproject/</project>\n" +
                "    <project>db/</project>\n" +
                "    <project>db_team_meeting/</project>\n" +
                "    <project>designate/</project>\n" +
                "    <project>distributed_virtual_router/</project>\n" +
                "    <project>distributed_virtual_routers/</project>\n" +
                "    <project>dnsaas/</project>\n" +
                "    <project>doc_team/</project>\n" +
                "    <project>doc_team_meeting/</project>\n" +
                "    <project>doc_web/</project>\n" +
                "    <project>doc_web_meeting/</project>\n" +
                "    <project>doc_web_team/</project>\n" +
                "    <project>doc_web_team_meeting/</project>\n" +
                "    <project>docteam/</project>\n" +
                "    <project>docteammeeting/</project>\n" +
                "    <project>docwebteam/</project>\n" +
                "    <project>ec2_api/</project>\n" +
                "    <project>fuel/</project>\n" +
                "    <project>gantt/</project>\n" +
                "    <project>glance/</project>\n" +
                "    <project>ha_guide/</project>\n" +
                "    <project>ha_guide_update/</project>\n" +
                "    <project>heat/</project>\n" +
                "    <project>hierarchical_multitenancy/</project>\n" +
                "    <project>hierarchical_multitenancy_/</project>\n" +
                "    <project>hierarchicalmultitenancy/</project>\n" +
                "    <project>honeybooboo/</project>\n" +
                "    <project>horizon/</project>\n" +
                "    <project>hyper_v/</project>\n" +
                "    <project>incub_sync/</project>\n" +
                "    <project>infra/</project>\n" +
                "    <project>ipv6_neutron/</project>\n" +
                "    <project>ironic/</project>\n" +
                "    <project>isviridov/</project>\n" +
                "    <project>keystone/</project>\n" +
                "    <project>keystone_meeting/</project>\n" +
                "    <project>kolla/</project>\n" +
                "    <project>large_deployment_team/</project>\n" +
                "    <project>large_deployment_team_january_2015_meeting/</project>\n" +
                "    <project>lbaas/</project>\n" +
                "    <project>lbaas_neutron/</project>\n" +
                "    <project>libvirt/</project>\n" +
                "    <project>log_wg/</project>\n" +
                "    <project>magentodb/</project>\n" +
                "    <project>magnetodb/</project>\n" +
                "    <project>magnetodb_bulk_load_import/</project>\n" +
                "    <project>magnetodb_daily_syncup/</project>\n" +
                "    <project>magnetodb_syncup_notes/</project>\n" +
                "    <project>magnetodb_weekly_meeting/</project>\n" +
                "    <project>manila/</project>\n" +
                "    <project>marconi/</project>\n" +
                "    <project>marconi_api/</project>\n" +
                "    <project>marconi_api_http/</project>\n" +
                "    <project>marconi_foo/</project>\n" +
                "    <project>marconi_notifications_brainstorm/</project>\n" +
                "    <project>marconi_project_team/</project>\n" +
                "    <project>marconi_roadmap/</project>\n" +
                "    <project>marconi_team/</project>\n" +
                "    <project>marconi_team_meeting/</project>\n" +
                "    <project>meetbot/</project>\n" +
                "    <project>milk/</project>\n" +
                "    <project>mistral/</project>\n" +
                "    <project>mistral_meeting/</project>\n" +
                "    <project>mistral_weekly_meeting/</project>\n" +
                "    <project>murani/</project>\n" +
                "    <project>murano/</project>\n" +
                "    <project>murano_bug_scrub/</project>\n" +
                "    <project>murano_community_meeting/</project>\n" +
                "    <project>murano_sincup/</project>\n" +
                "    <project>networking/</project>\n" +
                "    <project>networking_advance_services/</project>\n" +
                "    <project>networking_advanced_services/</project>\n" +
                "    <project>networking_fwaas/</project>\n" +
                "    <project>networking_l2gw/</project>\n" +
                "    <project>networking_ml2/</project>\n" +
                "    <project>networking_policy/</project>\n" +
                "    <project>networking_third_party_testing/</project>\n" +
                "    <project>networking_vpnaas/</project>\n" +
                "    <project>networkings/</project>\n" +
                "    <project>neutron/</project>\n" +
                "    <project>neutron_advanced_routing/</project>\n" +
                "    <project>neutron_db/</project>\n" +
                "    <project>neutron_drivers/</project>\n" +
                "    <project>neutron_ipv6/</project>\n" +
                "    <project>neutron_juno_3_bp_review/</project>\n" +
                "    <project>neutron_l3/</project>\n" +
                "    <project>neutron_lbaas/</project>\n" +
                "    <project>neutron_lbaas_subteam_meeting/</project>\n" +
                "    <project>neutron_nova_network_parity/</project>\n" +
                "    <project>neutron_qos/</project>\n" +
                "    <project>neutron_service_topic/</project>\n" +
                "    <project>neutron_servicevm/</project>\n" +
                "    <project>neutronlbaas/</project>\n" +
                "    <project>nfv/</project>\n" +
                "    <project>nova/</project>\n" +
                "    <project>nova_api/</project>\n" +
                "    <project>nova_api_wg/</project>\n" +
                "    <project>nova_bug_scrub/</project>\n" +
                "    <project>nova_cells/</project>\n" +
                "    <project>nova_net_to_neutron_migration/</project>\n" +
                "    <project>novabugscrub/</project>\n" +
                "    <project>nuetron_lbaas/</project>\n" +
                "    <project>octavia/</project>\n" +
                "    <project>openstack-barbican/</project>\n" +
                "    <project>openstack-chef/</project>\n" +
                "    <project>openstack-containers/</project>\n" +
                "    <project>openstack-dev/</project>\n" +
                "    <project>openstack-doc/</project>\n" +
                "    <project>openstack-ko/</project>\n" +
                "    <project>openstack-lbaas/</project>\n" +
                "    <project>openstack-marconi/</project>\n" +
                "    <project>openstack-meeting-3/</project>\n" +
                "    <project>openstack-meeting-4/</project>\n" +
                "    <project>openstack-meeting-alt/</project>\n" +
                "    <project>openstack-meeting/</project>\n" +
                "    <project>openstack-neutron/</project>\n" +
                "    <project>openstack-relmgr-office/</project>\n" +
                "    <project>openstack-sahara/</project>\n" +
                "    <project>openstack-sdks/</project>\n" +
                "    <project>openstack-sprint/</project>\n" +
                "    <project>openstack-trove/</project>\n" +
                "    <project>openstack/</project>\n" +
                "    <project>openstack_ansible/</project>\n" +
                "    <project>openstack_ansible_meeting/</project>\n" +
                "    <project>openstack_ceilometer/</project>\n" +
                "    <project>openstack_chef/</project>\n" +
                "    <project>openstack_community/</project>\n" +
                "    <project>openstack_core_meetup/</project>\n" +
                "    <project>openstack_i18n/</project>\n" +
                "    <project>openstack_i18n_meeting/</project>\n" +
                "    <project>openstack_i18n_test_meeting/</project>\n" +
                "    <project>openstack_networking__vpn_/</project>\n" +
                "    <project>openstack_networking_vpn/</project>\n" +
                "    <project>openstack_operators/</project>\n" +
                "    <project>openstack_orchestration_group/</project>\n" +
                "    <project>openstack_qa/</project>\n" +
                "    <project>openstack_sdk_php/</project>\n" +
                "    <project>openstack_secuirty_group/</project>\n" +
                "    <project>openstack_security_group/</project>\n" +
                "    <project>openstack_state_management/</project>\n" +
                "    <project>openstack_training/</project>\n" +
                "    <project>openstack_training_weekly_team_meeting/</project>\n" +
                "    <project>openstackclient/</project>\n" +
                "    <project>operations/</project>\n" +
                "    <project>oslo/</project>\n" +
                "    <project>ossg/</project>\n" +
                "    <project>ossg_weekly/</project>\n" +
                "    <project>passthrough/</project>\n" +
                "    <project>pci_meeting/</project>\n" +
                "    <project>pci_passthrough/</project>\n" +
                "    <project>pci_passthrough_meeting/</project>\n" +
                "    <project>pci_passthrough_network/</project>\n" +
                "    <project>poppy_team_meeting/</project>\n" +
                "    <project>poppy_weekly_meeting/</project>\n" +
                "    <project>product_team/</project>\n" +
                "    <project>project/</project>\n" +
                "    <project>ptl_sync/</project>\n" +
                "    <project>puppet_openstack/</project>\n" +
                "    <project>python3/</project>\n" +
                "    <project>python_openstack_sdk/</project>\n" +
                "    <project>python_openstacksdk/</project>\n" +
                "    <project>python_openstacksdk_weekly_meeting/</project>\n" +
                "    <project>python_openstacksdkk/</project>\n" +
                "    <project>qa/</project>\n" +
                "    <project>quantum/</project>\n" +
                "    <project>quantum_team_meeting/</project>\n" +
                "    <project>quantum_tempest/</project>\n" +
                "    <project>quantum_vpn/</project>\n" +
                "    <project>quantum_vpnaas/</project>\n" +
                "    <project>rally/</project>\n" +
                "    <project>random_test/</project>\n" +
                "    <project>reddwarf/</project>\n" +
                "    <project>refstack/</project>\n" +
                "    <project>sahara/</project>\n" +
                "    <project>satori/</project>\n" +
                "    <project>savanna/</project>\n" +
                "    <project>scheduler/</project>\n" +
                "    <project>scheduler_sub_group/</project>\n" +
                "    <project>scheduling/</project>\n" +
                "    <project>servicevm_device_manager/</project>\n" +
                "    <project>shara/</project>\n" +
                "    <project>sharding/</project>\n" +
                "    <project>show/</project>\n" +
                "    <project>solum/</project>\n" +
                "    <project>solum_team_meeting/</project>\n" +
                "    <project>stackalytics/</project>\n" +
                "    <project>state_management/</project>\n" +
                "    <project>state_management_goodness/</project>\n" +
                "    <project>storyboard/</project>\n" +
                "    <project>storyboard_weekly_meeting/</project>\n" +
                "    <project>swift/</project>\n" +
                "    <project>swift_community_meeting/</project>\n" +
                "    <project>syncup_140408/</project>\n" +
                "    <project>syncup_140409/</project>\n" +
                "    <project>syncup_140410/</project>\n" +
                "    <project>syncup_140414/</project>\n" +
                "    <project>syncup_140415/</project>\n" +
                "    <project>syncup_140418/</project>\n" +
                "    <project>taskflow/</project>\n" +
                "    <project>tc/</project>\n" +
                "    <project>telcowg/</project>\n" +
                "    <project>test/</project>\n" +
                "    <project>test2/</project>\n" +
                "    <project>test_eavesdrop/</project>\n" +
                "    <project>test_lurk/</project>\n" +
                "    <project>third_party/</project>\n" +
                "    <project>third_party_ci/</project>\n" +
                "    <project>third_party_ci_documentation/</project>\n" +
                "    <project>throwaway_vote/</project>\n" +
                "    <project>trainers/</project>\n" +
                "    <project>training_guides/</project>\n" +
                "    <project>training_manuals/</project>\n" +
                "    <project>training_mqni/</project>\n" +
                "    <project>transfer_service/</project>\n" +
                "    <project>tripleo/</project>\n" +
                "    <project>tripleo_weekly/</project>\n" +
                "    <project>trove/</project>\n" +
                "    <project>trove___reddwarf/</project>\n" +
                "    <project>trove_bp_meeting/</project>\n" +
                "    <project>trove_bp_review/</project>\n" +
                "    <project>tuskar/</project>\n" +
                "    <project>upstream_training/</project>\n" +
                "    <project>ux/</project>\n" +
                "    <project>vmwareapi/</project>\n" +
                "    <project>vmwareapi_subteam/</project>\n" +
                "    <project>vmwarepi/</project>\n" +
                "    <project>vpnaas/</project>\n" +
                "    <project>weekly_poppy_meeting/</project>\n" +
                "    <project>xen/</project>\n" +
                "    <project>xenapi/</project>\n" +
                "    <project>zaqar/</project>\n" +
                "</projects>";
        String url = "http://localhost:8080/assignment4/myeavesdrop/projects";


        //todo: count number of projects
        //todo: assert that there are 315 projects
        //todo: assert that project barbican exists
        //todo: assert that project %23openstack-api exists

        //Client client = ClientBuilder.newBuilder().build();
        WebTarget target = client.target(url);
        Response response = target.request().get();
        String xmloutput = response.readEntity(String.class);
        if(response.getStatus() == 404) throw new WebApplicationException(Response.Status.NOT_FOUND);
        //System.out.println(users);

        int projectCount = 0;
        boolean heatirc = false;
        boolean meetings = false;
        Document doc = Jsoup.parse(xmloutput, "", Parser.xmlParser());
        for (Element e : doc.select("project")) {
            //System.out.println(e);
            projectCount++;
            if(e.text().contains("%23heat/")) {
                heatirc = true;
            }
            if(e.text().contains("test_lurk/")) {
                meetings = true;
            }
        }
        assertEquals(projectCount, 315);
        assertTrue(heatirc);
        assertTrue(meetings);

        response.close();
    }

    @Test
    public void testEavesdropResourceGetProject() {
        String testProject = "%23openstack-api";
        Client client = ClientBuilder.newClient();
        String xml = "";
        String url = "http://localhost:8080/assignment4/myeavesdrop/projects/%23openstack-api/irclogs";

        WebTarget target = client.target(url);
        Response response = target.request().get();
        String xmloutput = response.readEntity(String.class);
        if(response.getStatus() == 404) throw new WebApplicationException(Response.Status.NOT_FOUND);
        //assert status code is not 404
        //assert that there are 32 links

        int linkCount = 0;
        boolean openstacklink1 = false;
        boolean openstacklink2 = false;
        Document doc = Jsoup.parse(xmloutput, "", Parser.xmlParser());
        for (Element e : doc.select("link")) {
            //System.out.println(e);
            linkCount++;
            if(e.text().contains("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-27.log")) {
                openstacklink1 = true;
            }
            if(e.text().contains("http://eavesdrop.openstack.org/irclogs/%23openstack-api/#openstack-api.2015-03-08.log")) {
                openstacklink2 = true;
            }
        }
        assertEquals(linkCount, 32);
        assertTrue(openstacklink1);
        assertTrue(openstacklink2);

        response.close();
    }

    @Test
    public void testEavesdropResourceGetProjectDoesNotExist() {
        //http://localhost:8080/assignment4/myeavesdrop/projects/non-existent-project/irclogs
        //assert that status code is 404
        Client client = ClientBuilder.newClient();
        String url = "http://localhost:8080/assignment4/myeavesdrop/projects/non-existent-project/irclogs";
        WebTarget target = client.target(url);
        Response response = target.request().get();
        String xmloutput = response.readEntity(String.class);
        /*if(response.getStatus() == 404) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }*/
        assertEquals(response.getStatus(),404);
    }
}
